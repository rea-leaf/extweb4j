package com.extweb4j.web.controller;

import java.util.List;

import com.extweb4j.core.controller.ExtController;
import com.extweb4j.core.kit.ExtKit;
import com.extweb4j.core.model.ExtIcon;
/**
 * 图标控制器
 * @author Administrator
 *
 */
public class IconController extends ExtController{
	
	public void json(){
		String query = getPara("query");
		List<ExtIcon> list = null;
		
		if(query!=null){
			query = ExtKit.sqlFilterValidate(query);
			list = ExtIcon.dao.findBy("icon_name like '%"+query+"%'");
		}else{
			list = ExtIcon.dao.findAll();
		}
		
		renderJson(list);
	}

}
