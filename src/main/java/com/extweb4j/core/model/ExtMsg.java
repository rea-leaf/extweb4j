package com.extweb4j.core.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.plugin.activerecord.Page;


/**
 * 系统消息
 * @author Administrator
 *
 */
public class ExtMsg extends CoreModel<ExtMsg>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static ExtMsg dao = new ExtMsg();

	public Page<ExtMsg> paginateByKeywaord(int page, int limit,String keywords) {
		// TODO Auto-generated method stub
		List<Object> paras = new ArrayList<Object>();
		
		StringBuffer sb = new StringBuffer(" 1=1 ");
		if(StringUtils.isNotBlank(keywords)){
			keywords = keywords.trim();
			sb.append("	and (msg_title like concat('%',?,'%') OR msg_content like concat('%',?,'%'))");
			paras.add(keywords);
			paras.add(keywords);
		}
		
		sb.append(" ORDER BY status ASC,level DESC,create_time DESC");
		
		System.out.println(sb.toString());
		return super.paginateBy(page, limit, sb.toString(),paras.toArray());
	}
	

}
