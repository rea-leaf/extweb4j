package com.extweb4j.core.model;

import java.util.List;
/**
 *角色菜单
 * @author Administrator
 *
 */
public class ExtRoleMenu extends CoreModel<ExtRoleMenu>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static ExtRoleMenu dao = new ExtRoleMenu();

	public List<ExtRoleMenu> findMenusByIn(List<String> rids) {
		// TODO Auto-generated method stub
		
		StringBuffer  sb = new StringBuffer("");
		for(int i=0;i<rids.size();i++){
			sb.append(" ? ").append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		return ExtRoleMenu.dao.find(" SELECT rm.menu_id FROM ext_role_menu rm WHERE rm.role_id IN ("+sb.toString()+")", rids.toArray());
	}
}
