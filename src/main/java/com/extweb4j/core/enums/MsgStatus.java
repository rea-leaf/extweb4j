package com.extweb4j.core.enums;
/**
 * 消息处理状态
 * @author Administrator
 *
 */
public enum  MsgStatus {
	
	待处理("0"), 已完成("1");
	
	private String state = "0";

	private MsgStatus(String state) {
		this.state = state;
	}

	public String  getState() {
		return state;
	}
}
