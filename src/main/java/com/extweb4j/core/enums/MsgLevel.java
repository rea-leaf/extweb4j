package com.extweb4j.core.enums;
/**
 * 消息级别
 * @author Administrator
 *
 */
public enum  MsgLevel {
	
	普通("0"), 警告("1");
	
	private String state = "0";

	private MsgLevel(String state) {
		this.state = state;
	}

	public String  getState() {
		return state;
	}
}
